package test;
import board.TetrisBoard;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


/**
 * Test Tetris board.
 * @author Michael
 * @version 14 November 2011
 */
public class BoardTest {
  
  /**
   * Seed for sequential game to simulate standard play.
   */
  private static final long SEED = 30;
  
  /**
   * String reused to represent empty area of board.
   */
  private static final String REPETITIVEBLANK = "[ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]";
  
  /**
   * String reused to represent area of board.
   */
  private static final String REPETITIVE_1 = " O [ ][ ][ ][ ][ ][ ][ ][ ][ ]";
  
  /**
   * String reused to represent area of board.
   */
  private static final String REPETITIVE_2 = "[X][ ][ ][ ][ ][ ][ ][ ][ ][ ]";
  
  /**
   * Test right boundary.
   */
  @Test
  public void testRight() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(SEED);
   
    moveLoop(Move.DOWN, 5, board); 
    moveLoop(Move.RIGHT, 7, board);

    // Build String for expected state of the board after piece pushes right boundary.
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 4; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    sb.append("[ ][ ][ ][ ][ ][ ] O  O  O  O ");
    sb.append('\n');
    for (int i = 0; i < 5; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    
    // testing right boundary.
    assertEquals("right boundary test", sb.toString(), board.toString());
  }
  
  /**
   * Test left boundary. 
   */
  @Test
  public void testLeft() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(SEED);
    
    moveLoop(Move.DOWN, 5, board);
    board.rotate();
    moveLoop(Move.LEFT, 10, board);

    // Build String for expected state of the board after piece pushes left boundary.
    final StringBuilder sb = new StringBuilder();
    sb.append(REPETITIVEBLANK);
    sb.append('\n'); 
    for (int i = 0; i < 4; i++) {
      sb.append(REPETITIVE_1);
      sb.append('\n');
    }
    for (int i = 0; i < 5; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    
    // testing left boundary.
    assertEquals("left boundary test", sb.toString(), board.toString());
  }
  
  /**
   * Test the collision of active piece with frozen piece in lateral contact.
   */
  @Test
  public void testCollision() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(SEED);

    moveLoop(Move.DOWN, 5, board);
    moveLoop(Move.RIGHT, 7, board);
    board.rotate();
    moveLoop(Move.LEFT, 10, board);
    moveLoop(Move.DOWN, 14, board);
    moveLoop(Move.LEFT, 6, board);
    
    // Build String for expected state of the board after active collides with frozen piece.
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 6; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    sb.append("[X] O [ ][ ][ ][ ][ ][ ][ ][ ]");
    sb.append('\n');
    sb.append("[X] O  O  O [ ][ ][ ][ ][ ][ ]");
    sb.append('\n');
    for (int i = 0; i < 2; i++) {
      sb.append(REPETITIVE_2);
      sb.append('\n');
    }
    
    // testing collision.
    assertEquals("collision test", sb.toString(), board.toString());
  }
  
  /**
   * Test that three pieces freeze properly. The sequence of IPiece, JPiece and ZPiece
   * are dropped to see if they properly land and freeze in place at bottom of board.
   */
  @Test
  public void testFreezeThree() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(SEED);

    moveLoop(Move.DOWN, 30, board);
    
    // Build String for expected state of the board after dropping pieces.
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 6; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    sb.append("[ ][ ][ ][ ][X][X][ ][ ][ ][ ]");
    sb.append('\n');
    sb.append("[ ][ ][ ][ ][X][X][X][ ][ ][ ]");
    sb.append('\n');
    sb.append("[ ][ ][ ][ ][X][X][X][ ][ ][ ]");
    sb.append('\n');
    sb.append("[ ][ ][ ][ ][X][X][X][X][ ][ ]");
    sb.append('\n');
    
    // test the freeze of three pieces.
    assertEquals("freeze three test", sb.toString(), board.toString());
  }

  /**
   * Test state of board before removal of one row.
   */
  @Test
  public void testSingleRowRemovalBefore() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(SEED);

    moveLoop(Move.DOWN, 5, board);
    moveLoop(Move.RIGHT, 7, board);
    board.rotate();
    moveLoop(Move.LEFT, 10, board);
    moveLoop(Move.DOWN, 14, board);
    moveLoop(Move.LEFT, 6, board);
    moveLoop(Move.DOWN, 13, board);
    board.moveLeft();
    moveLoop(Move.DOWN, 7, board);  
    board.moveRight();
    moveLoop(Move.DOWN, 10, board);
    moveLoop(Move.RIGHT, 5, board);
    
    // Build String for expected state of the board before removal.
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 3; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    sb.append("[ ][ ][ ][ ][ ][ ][ ] O  O [ ]");
    sb.append('\n');
    sb.append("[ ][ ][ ][ ][ ][ ][ ][ ] O  O ");
    sb.append('\n');
    sb.append(REPETITIVEBLANK);
    sb.append('\n');
    for (int i = 0; i < 2; i++) {
      sb.append(REPETITIVE_2);
      sb.append('\n');
    }
    sb.append("[X][X][ ][X][X][X][X][ ][ ][ ]");
    sb.append('\n');
    sb.append("[X][X][X][X][X][X][X][X][ ][ ]");
    sb.append('\n');

    // testing single row removal before.    
    assertEquals("single row removal test before", sb.toString(), board.toString());
  }
  
  /**
   * Test state of board after removal of one row.
   */
  @Test
  public void testSingleRowRemovalAfter() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(SEED);
    moveLoop(Move.DOWN, 5, board);
    moveLoop(Move.RIGHT, 7, board);
    board.rotate();
    moveLoop(Move.LEFT, 10, board);
    moveLoop(Move.DOWN, 14, board);
    moveLoop(Move.LEFT, 6, board);
    moveLoop(Move.DOWN, 13, board);
    board.moveLeft();
    moveLoop(Move.DOWN, 7, board);    
    board.moveRight();   
    moveLoop(Move.DOWN, 10, board);
    moveLoop(Move.RIGHT, 5, board);
    moveLoop(Move.DOWN, 6, board);

    // Build String for expected state of the board after removal.
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 7; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    for (int i = 0; i < 2; i++) {
      sb.append(REPETITIVE_2);
      sb.append('\n');
    }
    sb.append("[X][X][ ][X][X][X][X][X][X][ ]");
    sb.append('\n');

    // testing single row removal after.
    assertEquals("single row removal test after", sb.toString(), board.toString());
  }
  
  /**
   * Test state of board after removal of two consecutive rows.
   * The seed "96739730" was used to create a sequence of only I pieces for the next three
   * test methods.
   */
  @Test
  public void testTwoRowRemoval() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(96739730);

    moveLoop(Move.DOWN, 9, board);
    moveLoop(Move.LEFT, 5, board);
    moveLoop(Move.DOWN, 15, board);
    moveLoop(Move.LEFT, 5, board);
    moveLoop(Move.DOWN, 20, board);
    board.rotate();
    moveLoop(Move.RIGHT, 5, board);
    moveLoop(Move.DOWN, 10, board);
    board.rotate();
    moveLoop(Move.DOWN, 6, board);
    moveLoop(Move.RIGHT, 5, board);
    moveLoop(Move.DOWN, 4, board);
    
    // Build String for expected state of the board after removal of two rows.
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 8; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    for (int i = 0; i < 2; i++) {
      sb.append("[ ][ ][ ][ ][ ][ ][ ][ ][X][X]");
      sb.append('\n');
    }
    
    // testing the removal of two rows.  
    assertEquals("two row removal test", sb.toString(), board.toString());
  }
  
  /**
   * Test state of board after removal of three consecutive rows.
   */
  @Test
  public void testThreeRowRemoval() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(96739730);

    moveLoop(Move.DOWN, 9, board);
    moveLoop(Move.LEFT, 5, board);
    moveLoop(Move.DOWN, 15, board);
    moveLoop(Move.LEFT, 5, board);
    moveLoop(Move.DOWN, 30, board);
    moveLoop(Move.LEFT, 5, board);
    moveLoop(Move.DOWN, 10, board);
    board.rotate();
    moveLoop(Move.RIGHT, 5, board);
    moveLoop(Move.DOWN, 10, board);
    board.rotate();
    moveLoop(Move.RIGHT, 4, board);
    moveLoop(Move.DOWN, 8, board);
    
    // Build String for expected state of the board after removal.
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 9; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    sb.append("[ ][ ][ ][ ][ ][ ][ ][ ][X][X]");
    sb.append('\n');
    
    // testing the removal of three rows.  
    assertEquals("Three row removal test", sb.toString(), board.toString());
  }
  /**
   * Test the removal of two non-concurrent rows at once.
   */
  @Test
  public void testNonConcurrentLineRemoval() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(96739730);
    moveLoop(Move.DOWN, 9, board);
    moveLoop(Move.LEFT, 4, board);
    moveLoop(Move.DOWN, 15, board);
    moveLoop(Move.LEFT, 2, board);
    moveLoop(Move.DOWN, 20, board);
    moveLoop(Move.LEFT, 4, board);
    moveLoop(Move.DOWN, 10, board);
    board.rotate();
    moveLoop(Move.RIGHT, 4, board);
    moveLoop(Move.DOWN, 10, board);
    board.rotate();
    moveLoop(Move.RIGHT, 5, board);
    moveLoop(Move.DOWN, 7, board);
    
    // Build String for expected state of the board before removal.
    final StringBuilder sb1 = new StringBuilder();
    for (int i = 0; i < 6; i++) {
      sb1.append(REPETITIVEBLANK);
      sb1.append('\n');
    }
    sb1.append("[ ][ ][ ][ ][ ][ ][ ][ ][X] O ");
    sb1.append('\n');
    sb1.append("[X][X][X][X][X][X][X][X][X] O ");
    sb1.append('\n');
    sb1.append("[ ][ ][X][X][X][X][ ][ ][X] O ");
    sb1.append('\n');
    sb1.append("[X][X][X][X][X][X][X][X][X] O ");
    sb1.append('\n');
    
    // testing state of board just before removing two non-concurrent rows.
    assertEquals("non-concurrent row removal test before", sb1.toString(), board.toString());
    
    // advance piece the last move down to create two non-concurrent full rows.
    board.moveDown();
  
    // Build String for expected state of the board after removal. 
    final StringBuilder sb2 = new StringBuilder();
    for (int i = 0; i < 8; i++) {
      sb2.append(REPETITIVEBLANK);
      sb2.append('\n');
    }
    sb2.append("[ ][ ][ ][ ][ ][ ][ ][ ][X][X]");
    sb2.append('\n');
    sb2.append("[ ][ ][X][X][X][X][ ][ ][X][X]");
    sb2.append('\n');
    
    // testing state of board just after removing two non-concurrent rows.
    assertEquals("non-concurrent row removal test after", sb2.toString(), board.toString());

  }
  
  /**
   * Test the removal of four rows at once to simulate when "tetris" is obtained by player.
   * The seed "150594207" was used to create a sequence of eight O's and two I's.
   */
  @Test
  public void testFourRowRemoval() {
    final TetrisBoard board = new TetrisBoard(10, 10);
    board.sequentialGame(150594207);
    
    moveLoop(Move.DOWN, 25, board);
    moveLoop(Move.RIGHT, 4, board);
    moveLoop(Move.DOWN, 10, board);
    moveLoop(Move.RIGHT, 4, board);
    moveLoop(Move.DOWN, 10, board);
    moveLoop(Move.RIGHT, 2, board);
    moveLoop(Move.DOWN, 10, board);
    moveLoop(Move.RIGHT, 2, board);
    moveLoop(Move.DOWN, 10, board);
    moveLoop(Move.LEFT, 2, board);
    moveLoop(Move.DOWN, 10, board);
    moveLoop(Move.LEFT, 2, board);
    moveLoop(Move.DOWN, 7, board);
    board.rotate();
    moveLoop(Move.LEFT, 3, board);
    moveLoop(Move.DOWN, 12, board);   
    board.rotate();
    moveLoop(Move.LEFT, 4, board);
    moveLoop(Move.DOWN, 8, board);
    
    //Build string observe board just after removal of four rows. 
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 10; i++) {
      sb.append(REPETITIVEBLANK);
      sb.append('\n');
    }
    
    // testing four row removal after.
    assertEquals("four row removal test", sb.toString(), board.toString());
  }
  
  /**
   * Enumerator for movement of pieces.
   * @author Michael
   */
  public enum Move { RIGHT, LEFT, DOWN, ROTATE }
  
  /**
   * Helper method to maneuver pieces in test.
   * @param the_move Move
   * @param the_count integer
   * @param the_board TetrisBoard 
   */
  public void moveLoop(final Move the_move, final int the_count, final TetrisBoard the_board) {
    for (int i = 0; i < the_count; i++) {
      switch (the_move) {
        case DOWN : 
          the_board.moveDown();
          break;
        case LEFT :
          the_board.moveLeft();
          break;
        case RIGHT :
          the_board.moveRight();
          break;
        case ROTATE :
          the_board.rotate();
          break;
        default:
          break;
      }
    }
  }
}
