package board;



import java.awt.Color;



/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 1 - Tetris Pieces
 * 04 November 2011
 */

/**
 * IPiece class for Tetris game.
 * @author Michael
 * @version 31 October 2011
 */
public class IPiece extends Piece {
 
  /**
   * Two dimensional array to store rotations: 1) which rotation, 2) four points of rotation.
   */
  private static final ImmutablePoint[][] ROTATIONS = new ImmutablePoint[][]{{ 
      new ImmutablePoint(0, 0), new ImmutablePoint(1, 0), new ImmutablePoint(2, 0),
      new ImmutablePoint(3, 0)}, {new ImmutablePoint(0, 0), new ImmutablePoint(0, 1),
      new ImmutablePoint(0, 2), new ImmutablePoint(0, 3)}};
  
  /**
   * Color for IPiece.
   */
  private final Color my_color;
  
  /**
   * Construct a new IPiece object. 
   * @param the_board_location ImmutablePoint
   */
  public IPiece(final ImmutablePoint the_board_location) {
    super(ROTATIONS, the_board_location);
    my_color = Color.MAGENTA;
  }
  
  /**
   * Return color for IPiece.
   * @return Color
   */
  public Color getColor() {
    return my_color;
  }
}
 