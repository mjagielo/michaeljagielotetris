package board;




import java.awt.Color;
import java.util.Observable;
import java.util.Random;

import sound.SoundPlayer;

/**
 * Build a Tetris board.
 * @author Michael
 * @version 14 November 2011
 */
public class TetrisBoard extends Observable {
  
  /**
   * Constant for height of board.
   */
  private static final int ROWS = 20;
  
  /**
   * Constant for width of board.
   */
  private static final int COLS = 10;
  
  /**
   * Constant for minimum board size.
   */
  private static final int MIN_SIZE = 4;
  
  /** 
   * Constant for board calibration.
   */
  private static final int CALIBRATE_03 = 3;
  
  /** 
   * Constant for board calibration.
   */
  private static final int CALIBRATE_04 = 4;
  
  /**
   * Constant for increasing score.
   */
  private static final int SCORE_INCREMENT = 5;
  
  /**
   * Constant for scoring tetris.
   */
  private static final int TETRIS = 4;
  
  /**
   * Constant for scoring tetris.
   */
  private static final int TETRIS_BONUS = 100;
  
  /**
   * For random generation.
   */
  private Random my_rand;
  
  /**
   * For highest y marker.
   */
  private int my_highest_y = -1;
  
  /**
   * Two dimensional color array for board.
   */
  private Color[][] my_board;
  
  /**
   * Boolean instance field for game over state.
   */
  private boolean my_game_over;
  
  /**
   * Boolean for state of game, paused or not paused.
   */
  private boolean my_paused_state;
  
  /**
   * Array of pieces for random generation.
   */
  private final TetrisPiece [] my_list_of_pieces;
  
  /**
   * Current piece.
   */
  private TetrisPiece my_piece;
  
  /**
   * Next piece.
   */
  private TetrisPiece my_next_piece;
  
  /**
   * Width of current board.
   */
  private final int my_width;
  
  /**
   * Height of current board.
   */
  private final int my_height;
  
  /**
   * Score of game.
   */
  private int my_score;
  
  /**
   * Number of lines cleared.
   */
  private int my_lines_cleared;
  
  /**
   * Instance field for SoundPlayer.
   */
  private SoundPlayer my_sound_player; 
  
  /**
   * Default constructor.
   */
  public TetrisBoard() {
    this(COLS, ROWS);
  }
  
  /**
   * Constructor for different size boards.
   * @param the_width int
   * @param the_height int 
   */
  public TetrisBoard(final int the_width, final int the_height) {
    super();
    my_width = Math.max(the_width, MIN_SIZE); 
    my_height = Math.max(the_height, MIN_SIZE) + CALIBRATE_04;
    final int middle = my_width / 2; 
    final ImmutablePoint starting_point = new 
        ImmutablePoint(middle - 1, my_height - CALIBRATE_03);
    my_board = new Color [my_height][my_width];
    my_list_of_pieces = new TetrisPiece [] {new IPiece(starting_point),
      new JPiece(starting_point), new LPiece(starting_point), new OPiece(starting_point),
      new SPiece(starting_point), new TPiece(starting_point), new ZPiece(starting_point)};
    setUpBoard();
  }
  
  /**
   * Set boolean values, call randomGame, create new SoundPlayer.
   */
  private void setUpBoard() {
    randomGame();
    my_game_over = true;
    my_paused_state = false; 
    my_sound_player = new SoundPlayer();
  }

  /**
   * Generate the next piece for display.
   */
  private void generateNextPiece() {
    if (my_next_piece == null) {
      my_next_piece = my_list_of_pieces[my_rand.nextInt(my_list_of_pieces.length)];
      my_piece = my_next_piece;
    } 
    my_piece = my_next_piece;
    my_next_piece = my_list_of_pieces[my_rand.nextInt(my_list_of_pieces.length)];
  }
  
  /**
   * Return the next piece.
   * @return TetrisPiece
   */
  public TetrisPiece getNextPiece() {
    return my_next_piece;
  }
  
  /**
   * Drop active piece to the bottom of board.
   */
  public void dropPiece() {
    if (!my_paused_state && !my_game_over) {
      final int current_y = my_piece.getMyBoardLocation().y();
      do {
        moveDown();
      } while (my_piece.getMyBoardLocation().y() < current_y);
      my_sound_player.play("audio/bell_ring.wav"); 
    }
  }
  
  /**
   * Move piece down.
   */
  public void moveDown() {
    final TetrisPiece temp = my_piece.moveDown();
    
    if (!my_paused_state && !my_game_over) {
      if (temp.getMyBoardLocation().y() > 0 && !checkCollision(temp)) { 
        my_piece = temp;                                  
        setChanged();
      } else {
        freeze(my_piece.getMyPoints());
        findHighestY();
        my_piece = my_list_of_pieces[my_rand.nextInt(my_list_of_pieces.length)];
        generateNextPiece();
        setChanged();
      }
      notifyObservers();
    }
  }
  
  /**
   * Move piece left.
   */
  public void moveLeft() {
    if (!my_paused_state && !my_game_over) {
      final TetrisPiece temp = my_piece.moveLeft();
      if (checkBoardBounds(temp) && !checkCollision(temp)) {
        my_piece = temp;
        setChanged();
      }
      notifyObservers();
    }
  }
  
  /**
   * Move piece right.
   */
  public void moveRight() {
    if (!my_paused_state && !my_game_over) {
      final TetrisPiece temp = my_piece.moveRight();
      if (checkBoardBounds(temp) && !checkCollision(temp)) {
        my_piece = temp;
        setChanged();
      }
      notifyObservers();
    }
  }
  
  /**
   * Rotate piece.
   */
  public void rotate() {
    if (!my_paused_state && !my_game_over) {
      final TetrisPiece temp = my_piece.rotate();
      if (checkBoardBounds(temp) && !checkCollision(temp)) {
        my_piece = temp;
        setChanged();
        my_sound_player.play("audio/Depth Charge 2-SoundBible.com-338644910.wav"); 
      }
      notifyObservers();
    }
  }
  
  /**
   * Store the location of highest Y point, called only when piece is frozen.
   */
  public void findHighestY() {
    final ImmutablePoint[] temp_points = my_piece.getMyPoints();
    int temp = 0;
    for (ImmutablePoint p : temp_points) {
      temp = Math.max(temp, p.y());
    }
    
    my_highest_y = Math.max(my_highest_y, temp + my_piece.getMyBoardLocation().y());
    
    if (my_highest_y >= my_board.length) {
      my_game_over = true;
    }
  }
  
  /**
   * Return boolean my_game_over.
   * @return boolean my_game_over
   */
  public boolean isGameOver() {
    return my_game_over;
  }

  /**
   * Check coordinate on the board to see if it matches piece points, called in checkCollision.
   * @param the_points ImmutablePoint[]
   * @param the_point ImmutablePoint 
   * @return boolean
   */
  public boolean checkMatch(final ImmutablePoint[] the_points,
                            final ImmutablePoint the_point) {
    boolean result = false;
    for (ImmutablePoint p : the_points) {
      if (p.equals(the_point)) {
        result = true;
      }
    }
    return result;
  }
  
  /**
   * Collision detection for pieces. Run through array, 
   * check piece current points against frozen pieces.
   * @return boolean
   * @param the_piece TetrisPiece
   */
  public boolean checkCollision(final TetrisPiece the_piece) {
    boolean result = false;
    final ImmutablePoint[] temp_points = the_piece.getMyPoints();
    for (int i = 0; i < temp_points.length; i++) {
      temp_points[i] = temp_points[i].translate(the_piece.getMyBoardLocation().x(),
                                                the_piece.getMyBoardLocation().y());
    }
      // check relative locations of piece for conflicts
    for (int row = my_board.length - 1; row >= 0; row--) {
      for (int col = 0; col < my_board[0].length; col++) {
        if (my_board[row][col] != null && checkMatch(temp_points,
                                                     new ImmutablePoint(col, row))) {
          result = true;
        }
      }
    }
    return result;
  }
  
  /**
   * Ensure that piece remains on board with next move.
   * @return boolean
   * @param the_piece TetrisPiece
   */
  public boolean checkBoardBounds(final TetrisPiece the_piece) {
    boolean result = true;
    final ImmutablePoint[] temp_points = the_piece.getMyPoints();
    for (int i = 0; i < temp_points.length; i++) {
      temp_points[i] = temp_points[i].translate(the_piece.getMyBoardLocation().x(),
                                                the_piece.getMyBoardLocation().y());
      result = result && (temp_points[i].x() < my_width && temp_points[i].x() >= 0);
    } 
    return result;
  }

  /**
   * Adds the points of frozen piece to my_board.
   * @param the_points ImmutablePoint
   */
  public void freeze(final ImmutablePoint[] the_points) {
    final ImmutablePoint[] temp_points = new ImmutablePoint[the_points.length];
    my_score += SCORE_INCREMENT;
    my_sound_player.play("audio/metal_bang.wav"); 
    
    for (int i = 0; i < the_points.length; i++) {
      // translate points from relative to absolute coordinates
      temp_points[i] = the_points[i].translate(my_piece.getMyBoardLocation().x(),
                                                my_piece.getMyBoardLocation().y());

      if (my_highest_y > ROWS - 2) { 

        my_game_over = true;
      } else {

        // freeze the piece
        my_board[temp_points[i].y()][temp_points[i].x()] = my_piece.getColor();
      }
    }
    updateBoard();
  }

  /**
   * Update game state by one step.
   */
  public void update() {
    moveDown(); 
  }
  
  /**
   * Check for full rows in game board, if full remove and add new row to top.
   */
  public void updateBoard() {
    int count_row = 0;
    int row = 0;
    while (row < my_board.length) {
      boolean full = true;
      // check row
      for (int col = 0; col < my_board[row].length && full; col++) {
        full = full && my_board[row][col] != null;
      }
      if (full) { 
          // delete current row
        System.arraycopy(my_board, row + 1, my_board, row, my_board.length - row - 1);
        // add new row at top
        my_board[my_board.length - 1] = new Color[my_width];
        row--; 
        count_row++;
        my_sound_player.play("audio/Laser_Cannon-Mike_Koenig-797224747.wav");
        my_lines_cleared++;
        my_score += SCORE_INCREMENT * 2;
      }
      row++;
    }
    if (count_row == TETRIS) {
      my_sound_player.play("audio/tetris.wav");
      my_score += TETRIS_BONUS; 
    }
  }

  /**
   * Representation of all filled squares.
   * @return String
   */
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    final ImmutablePoint[] temp_points = my_piece.getMyPoints();
    for (int i = 0; i < temp_points.length; i++) {
      temp_points[i] = temp_points[i].translate(my_piece.getMyBoardLocation().x(),
                                                my_piece.getMyBoardLocation().y());
    }
    for (int row = my_board.length - 1; row >= 0; row--) {
      for (int col = 0; col < my_board[row].length; col++) {
        if (checkMatch(temp_points, new ImmutablePoint(col, row))) {
          sb.append(" O ");
        } else if (my_board[row][col] != null) {
          sb.append("[X]");
        } else {
          sb.append("[ ]");
        }
      }
      sb.append("\n");
    }
    return sb.toString();
  }
  
  /**
   * Create new unseeded random generator for random game.
   */
  private void randomGame() {
    my_rand = new Random();
    generateNextPiece();
    setChanged();
    notifyObservers();
  }
    
  /**
   * Re-set random with new seed, can be used in GUI to re-seed game
   * for sequential play.
   * @param the_seed long 
   */
  public void sequentialGame(final long the_seed) {
    my_rand.setSeed(the_seed);
    generateNextPiece();
    setChanged();
    notifyObservers();
  }
  
  /**
   * Return clone of boolean board array.
   * @return boolean[][] 
   */
  public Color[][] getBoard() { 
    return my_board.clone();
  }
  
  /**
   * Return the tetris piece.
   * @return TetrisPiece
   */
  public TetrisPiece getPiece() {
    return my_piece;
  }

  /**
   * Return the integer value for the highest y point.
   * @return integer
   */
  public int getMyHighestY() {
    return my_highest_y;
  }
  
  /**
   * Return the sound player.
   * @return SoundPlayer
   */
  public SoundPlayer getMySoundPlayer() {
    return my_sound_player;
  }
  
  /**
   * Return score of game.
   * @return integer
   */
  public int getMyScore() {
    return my_score;
  }
  
  /**
   * Return number of lines cleared.
   * @return integer.
   */
  public int getMyLinesCleared() {
    return my_lines_cleared;
  }
  
  /**
   * Reset game, clearing board and score. 
   */
  public void resetGame() {
    my_game_over = false;
    my_board = new Color [my_height][my_width];
    generateNextPiece();
    my_highest_y = -1;
    my_paused_state = false;
    my_score = 0;
    my_lines_cleared = 0;
  }
  
  /**
   * Return boolean for paused state of game.
   * @return boolean
   */
  public boolean isPausedState() {
    return my_paused_state;
  }
  
  /**
   * Set boolean for paused state of game.
   * @param the_paused boolean
   */
  public void setPaused(final boolean the_paused) { 
    my_paused_state = the_paused;
  }
}
