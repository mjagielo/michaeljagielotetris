package board;


/**
 * Point object used for Tetris Piece class objects.
 * @author Michael
 * @version 1 November 2011
 */
public class ImmutablePoint {

  /**
   * The origin.
   */
  public static final ImmutablePoint ORIGIN =
    new ImmutablePoint(0, 0);
    
  /**
   * The x-coordinate.
   */
  private final int my_x;
    
  /**
   * The y-coordinate.
   */
  private final int my_y;
    
  /**
   * Construct a point with the specified coordinates.
   * @param the_coord_1 The first coordinate (x).
   * @param the_coord_2 The second coordinate (y)
   */
  public ImmutablePoint(final int the_coord_1, final int the_coord_2) {
    my_x = the_coord_1;
    my_y = the_coord_2;
  }
  
  /**
   * @return What is your x-coordinate?
   */
  public int x() {
    return my_x;
  }

  /**
   * @return What is your y-coordinate?
   */
  public int y() {
    return my_y;
  }
    
  /**
   * Translates this point by (the_x, the_y).
   * @param the_x The x to translate by.
   * @param the_y The y to translate by.
   * @return the resulting point.
   */
  public ImmutablePoint translate(final int the_x, final int the_y) {
    return new ImmutablePoint(my_x + the_x, my_y + the_y);  
  }
    
  /**
   * {@inheritDoc}
   * @Override 
   */
  public boolean equals(final Object the_other) {
    boolean result = false;
    if (this == the_other) {
      result = true;
    } else if (the_other != null && the_other.getClass() == getClass()) {
      final ImmutablePoint point = (ImmutablePoint) the_other;
      result = my_x == point.my_x && my_y == point.my_y;
    }
      
    return result;
  }
    
  /**
   * {@inheritDoc}
   * @Override
   */
  public int hashCode() {
    return toString().hashCode();
  }
    
  /**
   * {@inheritDoc}
   * @Override
   */
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append('(');
    sb.append(my_x);
    sb.append(", ");
    sb.append(my_y);
    sb.append(')');
    return sb.toString();
  }
}
