package board;



import java.awt.Color;




/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 1 - Tetris Pieces
 * 04 November 2011
 */

/**
 * AbstractPiece class for Tetris game.
 * @author Michael
 * @version 31 October 2011
 */
public abstract class Piece implements TetrisPiece, Cloneable {
  
  /**
   * Constant for boolean matrix, used in toString method. 
   */
  private static final int MATRIX_DIMENSION = 4;
  
  /**
   * Two dimension array stores rotations for piece.
   */
  private final ImmutablePoint[][] my_points;
  
  /**
   * Point for absolute board coordinate.
   */
  private ImmutablePoint my_board_location;
  
  /**
   * For reference in rotations array.
   */
  private int my_rotation_index;
  
  /**
   * Instance field for tetris piece.
   */
  private TetrisPiece my_piece;
  
  /**
   * Constructor.
   * @param the_rotations ImmutablePoint[][]
   * @param the_board_location ImmutablePoint
   */
  public Piece(final ImmutablePoint[][] the_rotations,
                       final ImmutablePoint the_board_location) {
    
    my_points = the_rotations.clone();
    my_board_location = the_board_location;
    my_rotation_index = 0;
  }
  
  /**
   * {@inheritDoc}
   * @return String 
   * @override java.lang.Object.toString 
   */
  
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    final boolean[][] matrix = new boolean[MATRIX_DIMENSION][MATRIX_DIMENSION];
    for (int i = 0; i < my_points[my_rotation_index].length; i++) {   
      final int x = my_points[my_rotation_index][i].x();
      final int y = my_points[my_rotation_index][i].y();
      matrix[y][x] = true;
    }
    
    for (int row = matrix.length - 1; row >= 0; row--) {
      for (int col = 0; col < matrix[0].length; col++) {
        if (matrix[row][col]) {
          sb.append("[ ]");

        } else {
          sb.append("   ");
        }
      }
      sb.append("\n");
    }
    return sb.toString();
  }
  
  /**
   * {@inheritDoc}
   * @return ImmutablePoint 
   */
  public ImmutablePoint getMyBoardLocation() {
    return my_board_location;
  }
  
  /**
   * Move piece left.
   * @return TetrisPiece
   */
  public TetrisPiece moveLeft() {
    final ImmutablePoint new_board_location = new ImmutablePoint(getMyBoardLocation().x() - 1,
                                                                 getMyBoardLocation().y());
    
    Piece moved = null;
    try {
      moved = clone();
    } catch (final CloneNotSupportedException ex) { assert false; }
      
    if (moved != null) {
      moved.my_board_location =  new_board_location;


    }
    return moved;
  }
  
  /**
   * Move piece right.
   * @return TetrisPiece
   */
  public TetrisPiece moveRight() {
    final ImmutablePoint new_board_location = new ImmutablePoint(getMyBoardLocation().x() + 1,
                                                                 getMyBoardLocation().y());
    
    Piece moved = null;
    try {
      moved = clone();
    } catch (final CloneNotSupportedException ex) { assert false; }
      
    if (moved != null) {
      moved.my_board_location =  new_board_location;

    }
    return moved;
  }
  
  /**
   * Move piece down.
   * @return TetrisPiece
   */
  public TetrisPiece moveDown() {

    final ImmutablePoint new_board_location = new ImmutablePoint(getMyBoardLocation().x(),
                                                                 getMyBoardLocation().y() - 1);
    
    Piece moved = null;
    try {
      moved = clone();
    } catch (final CloneNotSupportedException ex) { assert false; }
      
    if (moved != null) {
      moved.my_board_location =  new_board_location;
    }
    return (TetrisPiece) moved;
  }
  
  /**
   * Rotate piece.
   * @return TetrisPiece
   */
  public TetrisPiece rotate() { 
    Piece moved = null;
    try {
      moved = clone();
    } catch (final CloneNotSupportedException ex) { assert false; }
    
    if (moved != null) {
      moved.my_rotation_index =  my_rotation_index + 1;
    
// reset my_rotation_index counter if reached the end of MY_ROTATIONS array.
      if (moved.my_rotation_index == my_points.length) {
        moved.my_rotation_index = 0;
      }
    }
    return moved;
  }
  
  /**
   * Clone current piece.
   * @return AbstractPiece
   * @throws CloneNotSupportedException
   */@Override 
  public Piece clone() throws CloneNotSupportedException {
    return (Piece) super.clone();
  }
  
  /**
   * Return my_points array.
   * @return ImmutablePoint
   */
  public ImmutablePoint[] getMyPoints() {
    return my_points[my_rotation_index].clone();
  }
  
  /**
   * Return tetris piece.
   * @return TetrisPiece
   */
  public TetrisPiece getPiece() {
    return my_piece;
  }
  
  /**
   * Return color of tetris piece.
   * @return Color
   */
  public abstract Color getColor();
  
}
