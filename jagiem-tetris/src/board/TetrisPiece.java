package board;


import java.awt.Color;


/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 1 - Tetris Pieces
 * 04 November 2011
 */

/**
 * 
 * @author Mike
 * @version 2 November 2011
 */
public interface TetrisPiece {
  
  /**
   * Rotate TetrisPiece 90 degrees clockwise.
   * @return TetrisPiece
   */
  TetrisPiece rotate();
  
  /**
   * Move TetrisPiece left one space.
   * @return TetrisPiece 
   */
  TetrisPiece moveLeft();
  
  /**
   * Move TetrisPiece right one space.
   * @return TetrisPiece after moving
   */
  TetrisPiece moveRight();
  
  /**
   *  Move TetrisPiece down one space.
   * @return TetrisPiece
   */
  TetrisPiece moveDown();
  
  /**
   * Return String of 2D text drawing of TetrisPiece state for debugging.
   * @return String
   */
  String toString();

  /**
   * Return point.
   * @return ImmutablePoint
   */
  ImmutablePoint getMyBoardLocation();
  
  /**
   * return set of points for current piece.
   * @return ImmutablePoint
   */
  ImmutablePoint[] getMyPoints();
  
  /**
   * Return color of tetris piece.
   * @return Color
   */
  Color getColor();
  
  /**
   * Return tetris piece.
   * @return TetrisPiece
   */
  TetrisPiece getPiece();
}
