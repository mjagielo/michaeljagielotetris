package board;



import java.awt.Color;



/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 1 - Tetris Pieces
 * 04 November 2011
 */

/**
 * IPiece class for Tetris game.
 * @author Michael
 * @version 2 November 2011
 */
public class OPiece extends Piece {
  
  /**
   * Two dimensional array to store rotations: 1) which rotation, 2) four points of rotation.
   * Only one rotation is stored here as the OPiece does not visibly rotate.
   */
  private static final ImmutablePoint[][] ROTATIONS = new ImmutablePoint[][]{{ 
      new ImmutablePoint(0, 0), new ImmutablePoint(1, 0), new ImmutablePoint(1, 1),
      new ImmutablePoint(0, 1)}}; 
  /**
   * Color for OPiece.
   */
  private final Color my_color;
  
  /**
   * Construct a new OPiece object. 
   * @param the_board_location ImmutablePoint
   */
  public OPiece(final ImmutablePoint the_board_location) {
    super(ROTATIONS, the_board_location);
    my_color = Color.YELLOW;
  }
  
  /**
   * Return color for OPiece.
   * @return Color
   */
  public Color getColor() {
    return my_color;
  }
}