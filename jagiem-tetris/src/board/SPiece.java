package board;


import java.awt.Color;


/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 1 - Tetris Pieces
 * 04 November 2011
 */

/**
 * SPiece class for Tetris game.
 * @author Michael
 * @version 2 November 2011
 */
public class SPiece extends Piece {
  
  /**
   * Two dimensional array to store rotations: 1) which rotation, 2) four points of rotation.
   */
  private static final ImmutablePoint[][] ROTATIONS = new ImmutablePoint[][]{{ 
      new ImmutablePoint(0, 0), new ImmutablePoint(1, 0), new ImmutablePoint(1, 1),
      new ImmutablePoint(2, 1)}, {new ImmutablePoint(1, 0), new ImmutablePoint(1, 1),
      new ImmutablePoint(0, 1), new ImmutablePoint(0, 2)}};
  
  /**
   * Color for SPiece.
   */
  private final Color my_color;

  /**
   * Construct a new SPiece object. 
   * @param the_board_location ImmutablePoint
   */
  public SPiece(final ImmutablePoint the_board_location) {
    super(ROTATIONS, the_board_location);
    my_color = Color.GREEN;
  }
  
  /**
   * Return color for SPiece.
   * @return Color
   */
  public Color getColor() {
    return my_color;
  }
  

}



