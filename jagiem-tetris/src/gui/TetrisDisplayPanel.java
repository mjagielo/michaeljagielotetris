package gui;

import board.ImmutablePoint;
import board.TetrisBoard;
import board.TetrisPiece;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;





/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 3 - Initial GUI
 * 30 November 2011
 */

/**
 * Drawing panel class graphical display of Tetris game.
 * @author Michael
 * @version 30 November 2011
 *
 */
@SuppressWarnings("serial")
public class TetrisDisplayPanel extends JPanel implements Observer {
  
  /**
   * Constant for preferred setting for width of game panel.
   */
  private static final int DEFAULT_WIDTH = 300;
  
  /**
   * Constant for preferred setting for height of game panel.
   */
  private static final int DEFAULT_HEIGHT = 610;
  
  /**
   * Constant used for calculating display of pieces.
   */
  private static final int NUM_COLS = 10;
  
  /**
   * Constant used for calculating display of pieces.
   */
  private static final int NUM_ROWS = 20;
  
  /**
   * Instance field for TetrisBoard.
   */
  private final TetrisBoard my_board;
  
  /**
   * Instance of board array to draw board.
   */
  private Color[][] my_board_array;
  
  /**
   * Instance field for TetrisPiece.
   */
  private TetrisPiece my_piece;
  
  /**
   * Constructor.
   */
  public TetrisDisplayPanel() {
    super();
    setPreferredSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
    setBackground(Color.LIGHT_GRAY); 
    my_board = new TetrisBoard();
    my_piece = my_board.getPiece();
    my_board_array = my_board.getBoard().clone();
    requestFocus();    
  }

  /**
   * Draw on the DrawingPanel.
   * @param the_graphics Graphics
   * @Override
   */
  public void paintComponent(final Graphics the_graphics) {
    super.paintComponent(the_graphics);
    final Graphics2D g2d = (Graphics2D) the_graphics;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    final int width;
    final int height;
    width = getBounds().width / my_board_array[0].length; 
    height = getBounds().height / my_board_array.length; 
    final int length = Math.min(width, height); 
   
    
    //paint background of board
    g2d.setPaint(Color.DARK_GRAY);
    g2d.fill(new Rectangle(0, 0, NUM_COLS * length - 1, NUM_ROWS * length - 1));
    
    // draw frozen pieces
    g2d.setPaint(Color.BLACK);
    if (my_board_array != null) {
      for (int row = NUM_ROWS; row >= 0; row--) { 
        for (int col = 0; col < my_board_array[0].length; col++) {
          if (my_board_array[row][col] != null) { 
            g2d.setPaint(my_board_array[row][col]); 
            drawBlock(g2d, col * length, (NUM_ROWS - row) * length, length, length);
          }
        }
      }
    }
    // translate points and draw active piece
    final ImmutablePoint[] temp_points = my_piece.getMyPoints(); 
    for (int i = 0; i < temp_points.length; i++) {
      g2d.setPaint(my_piece.getColor());
      drawBlock(g2d,
                (temp_points[i].x() + my_piece.getMyBoardLocation().x()) * length,
                (NUM_ROWS - (temp_points[i].y() + my_piece.getMyBoardLocation().y())) * length,
                  length,
                  length);

    }
    drawGrid(NUM_COLS * length,
             NUM_ROWS * length, length, g2d, 1f);
  }

  /**
   * Draw block for tetris piece.
   * @param the_g integer
   * @param the_x integer
   * @param the_y integer
   * @param the_width integer
   * @param the_height integer
   */
  private void drawBlock(final Graphics2D the_g, final int the_x, final int the_y,
                         final int the_width, final int the_height) {
    the_g.fill3DRect(the_x, the_y, the_width, the_height, true);
    the_g.setPaint(Color.BLACK);
    the_g.drawRect(the_x, the_y, the_width, the_height);
  }
  
  /**
   * Draw grids on tetris display panel. 
   * @param the_width integer
   * @param the_height integer
   * @param the_size integer
   * @param the_g Graphics2D
   * @param the_thickness float
   */
  public void drawGrid(final int the_width, final int the_height,
                        final int the_size, final Graphics2D the_g,
                        final float the_thickness) {
    the_g.setStroke(new BasicStroke(the_thickness));
    the_g.setColor(Color.LIGHT_GRAY);
    
    // draw rows
    for (int i = 0; i <= the_height / the_size; i++) {
      the_g.drawLine(0, i * the_size, the_width, i * the_size);
    }
    
    // draw columns
    for (int i = 0; i <= the_width / the_size; i++) {
      the_g.drawLine(i * the_size, 0, i * the_size, the_height);
    }
    
    // draw border around grid
    the_g.setPaint(Color.RED);
    the_g.setStroke(new BasicStroke(1f));
    the_g.draw(new Rectangle(0, 0, NUM_COLS * the_size - 1, NUM_ROWS * the_size - 1));
  }

  @Override
  public void update(final Observable the_source, final Object the_data) {
    if (the_source instanceof TetrisBoard) {
      final TetrisBoard board = (TetrisBoard) the_source;
      my_board_array = board.getBoard();
      my_piece = board.getPiece();
    }
    repaint();
  }
}

  

