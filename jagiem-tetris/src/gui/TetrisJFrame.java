package gui;

import board.TetrisBoard;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;






/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 3 - Initial GUI
 * 30 November 2011
 */

/**
 * Create JFrame for graphical display of Tetris game.
 * @author Michael
 * @version 30 November 2011
 *
 */
@SuppressWarnings("serial")
public class TetrisJFrame extends JFrame  implements ActionListener, KeyListener, Observer {
  
  
  /**
   * Constant for preferred setting for width of game panel.
   */
  private static final int DEFAULT_WIDTH = 300;
  
  /**
   * Constant for preferred setting for height of JFrame.
   */
  private static final int DEFAULT_HEIGHT = 600;
  
  /**
   * Default width for panel.
   */
  private static final int DEFAULT_PANEL_WIDTH = 316;
  
  /**
   * Default height for panel.
   */
  private static final int DEFAULT_PANEL_HEIGHT = 741;
  
  /**
   * Constant to calculate alternative panel size.
   */
  private static final float DIVISOR = 0.70f;

  /**
   * Constant for setting of alternate width of JFrame.
   */
  private static final int WIDTH_2 = 200;
  
  /**
   * Constant for setting of alternate height of game panel.
   */
  private static final int HEIGHT_2 = 400; 
  
  /**
   * Constant field for timer speed.
   */
  private static int SPEED = 500;
  
  /**
   * Constant used to calculate the delay of the timer.
   */
  private static final int REQ_LINES = 10;
  
  /**
   * Constant used to calculate the delay of the timer.
   */
  private static final int DECREMENT = 10;
  
  /**
   * Constant used to display label for score.
   */
  private static final int LABEL_OFFSET = 6;
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_1 = "Welcome to Submarine Tetris!\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_2 = "To start  a new game," +
      " choose File / Start Game from the menu.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_3 = "Left arrow key moves piece left.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_4 = "Right arrow key moves piece right.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_5 = "Return key rotates the piece.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_6 = "Space bar drops the piece " +
      "immediately to the bottom.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_7 = "P button pauses the game.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_8 = "C button continues the game.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_9 = "The difficulty is increased with" +
      " each cleared line.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_10 = "5 points per piece frozen.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_11 = "10 points per line cleared.\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_12 = "100 bonus points for Tetris (clearing" +
      " four lines in a row).\n";
  
  /**
   * Constant string for greeting.
   */
  private static final String GREETING_TEXT_13 = "Scoring:\n";
  
  /**
   * Constant string for score labels.
   */
  private static final String BLANK = "     ";
  
  /**
   * Constant field for game timer.
   */
  private Timer my_timer;
  
  /**
   * Instance field for tetris display panel.
   */
  private TetrisDisplayPanel my_board_display;

  /**
   * Instance field for image label.
   */
  private JLabel my_pic_label;
  
  /**
   * Instance field for image.
   */
  private BufferedImage my_picture_full;
  
  /**
   * Instance field for image.
   */
  private Image my_picture_small;
  
  /**
   * Instance field for Tetris board.
   */
  private TetrisBoard my_board; 
  
  /**
   * Instance field for score label.
   */
  private JLabel my_score_label; 
  
  /**
   * Instance field for lines cleared label.
   */
  private JLabel my_lines_cleared_label; 
  
  /**
   * Constructor. 
   */
  public TetrisJFrame() {
    super("Tetris");
    setupFrame(); 
    displayImage(); 
    pack();
  }
  
  /**
   * Set up tetris JFrame.
   */
  private void setupFrame() {
    my_board_display = new TetrisDisplayPanel();
    my_board_display.addKeyListener(this);
    add(my_board_display, BorderLayout.CENTER); 
    setJMenuBar(createMenuBar());
    
    final PieceDisplayPanel piece_display = new PieceDisplayPanel();
    add(piece_display, BorderLayout.SOUTH);
    pack();
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    my_board = new TetrisBoard();
    my_board.addObserver(this);
    
    final JPanel score_display = new JPanel(new GridLayout(LABEL_OFFSET, 1));
    score_display.setBackground(Color.LIGHT_GRAY);
    my_score_label = new JLabel("Score:");
    score_display.add(my_score_label);
    my_lines_cleared_label = new JLabel("Lines Cleared:");
    score_display.add(my_lines_cleared_label);
    add(score_display, BorderLayout.EAST);
    
    my_board.addObserver(my_board_display);
    my_board.addObserver(piece_display); 
    my_board_display.requestFocusInWindow();
    final ActionListener listener = new GameTimer(); 
    my_timer = new Timer(SPEED, listener); 
    
    setVisible(true);
    setResizable(false);
  }

  /**
   * Display logo image for game.
   */
  private void displayImage() {
    
    final JPanel image_display = new JPanel();

    try {
      final URL path = TetrisJFrame.class.getResource("../images/submarine-silhouette.png");
      my_picture_full = ImageIO.read(path);
    } catch (final IOException e) {
      e.printStackTrace();
    } 
    my_picture_small =
        my_picture_full.getScaledInstance(Math.round(my_picture_full.getWidth() * DIVISOR),
                                          Math.round(my_picture_full.getHeight() * DIVISOR),
                                          Image.SCALE_SMOOTH);
    my_pic_label = new JLabel(new ImageIcon(my_picture_full)); 
    image_display.add(my_pic_label);
    image_display.setBackground(Color.LIGHT_GRAY);
    add(image_display, BorderLayout.WEST);
  }
  
  /**
   * Main method for Tetris.
   * @param the_args ignored.
   */
  public static void main(final String... the_args) {
    
    try {
      // set cross-platform Java look and feel
      UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
    } catch (final UnsupportedLookAndFeelException e) {
      e.printStackTrace(); 
    } catch (final ClassNotFoundException e) {
      e.printStackTrace();
    } catch (final InstantiationException e) {
      e.printStackTrace();
    } catch (final IllegalAccessException e) {
      e.printStackTrace();
    }
    new TetrisJFrame();
    greeting();
  } 
  
  /**
   * Start  game timer, which calls update method for game.
   */
  public void startGame() {
    my_board.resetGame();
    my_timer.start();
    my_board.getMySoundPlayer().play("audio/sonar.wav"); 
  }
  
  /**
   * Stop game timer, display message, end game.
   */
  public void gameOver() {
    my_board.getMySoundPlayer().play("audio/sonic.wav"); 
    my_timer.stop();
    javax.swing.JOptionPane.showMessageDialog(this, "Game Over!");
  }   
  
  @Override
  public void dispose() {
    if (my_timer != null && my_timer.isRunning()) {
      my_timer.stop();
    }
    super.dispose();
  }
  
  /**
   * Set up initial greeting.
   */
  public static void greeting() {
    final StringBuilder sb = new StringBuilder();
    sb.append(GREETING_TEXT_1);
    sb.append(GREETING_TEXT_2);
    sb.append(GREETING_TEXT_3);
    sb.append(GREETING_TEXT_4);
    sb.append(GREETING_TEXT_5);
    sb.append(GREETING_TEXT_6);
    sb.append(GREETING_TEXT_7);
    sb.append(GREETING_TEXT_8);
    sb.append(GREETING_TEXT_9);
    sb.append(GREETING_TEXT_13);
    sb.append(GREETING_TEXT_10);
    sb.append(GREETING_TEXT_11);
    sb.append(GREETING_TEXT_12);
    
    javax.swing.JOptionPane.showMessageDialog(null, sb);
  }

  /**
   * Helper method for file menu setup.
   * @param the_file_menu JMenu
   */
  private void setupFileMenuItems(final JMenu the_file_menu) {
    //
    final JMenuItem start_menu_item = new JMenuItem("Start Game");
    start_menu_item.setMnemonic(KeyEvent.VK_S);
    start_menu_item.setToolTipText("Start a new game");
    the_file_menu.add(start_menu_item);
    start_menu_item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_event) {
        startGame();  
      }
    });

    // File
    the_file_menu.setMnemonic(KeyEvent.VK_F);
    final JMenuItem quit_menu_item = new JMenuItem("Quit");
    quit_menu_item.setMnemonic(KeyEvent.VK_Q);
    quit_menu_item.setToolTipText("Quit Tetris");
    the_file_menu.add(quit_menu_item);
    quit_menu_item.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent the_event) {
          dispose();
        } 
      });
  }

  /**
   * Helper method for board size menu setup. 
   * @param the_board_size_menu JMenu
   */
  private void setupBoardSizeMenuItems(final JMenu the_board_size_menu) {
    final Icon at_icon = new ImageIcon("at.gif");
    the_board_size_menu.setIcon(at_icon);
    the_board_size_menu.setMnemonic(KeyEvent.VK_B);
    the_board_size_menu.setToolTipText("Change the board size");
    final ButtonGroup board_size_menu_item_group = new ButtonGroup();

    for (int i = 1; i <= 2; i++) {
      final JRadioButtonMenuItem current_board_size = new
          JRadioButtonMenuItem(String.valueOf(i));
      final BoardSizeAction board_size_action = new BoardSizeAction(i);
      current_board_size.addActionListener(board_size_action);
      current_board_size.setMnemonic(String.valueOf(i).charAt(0));
      board_size_menu_item_group.add(current_board_size);
      the_board_size_menu.add(current_board_size);
      if (i == 1) { 
        current_board_size.setSelected(true);
      }
    }
  }
  
  /**
   * Set board size.
   * @param the_size integer 
   */
  public void setBoardSize(final int the_size) {
    if (the_size == 1) {
      my_board_display.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
      setSize(DEFAULT_PANEL_WIDTH, DEFAULT_PANEL_HEIGHT);
      
      my_board_display.setPreferredSize(new Dimension(DEFAULT_PANEL_WIDTH,
                                                      DEFAULT_PANEL_HEIGHT));
      my_pic_label.setIcon(new ImageIcon(my_picture_full));
    } else {
      my_board_display.setSize(WIDTH_2, HEIGHT_2);
      my_board_display.setPreferredSize(new Dimension(WIDTH_2,
                                                      HEIGHT_2));
      my_pic_label.setIcon(new ImageIcon(my_picture_small));
    }
    pack();
  }

  /**
   * Helper method for help menu setup.
   * @param the_help_menu JMenu
   */
  private void setupHelpMenuItems(final JMenu the_help_menu) {
    the_help_menu.setMnemonic(KeyEvent.VK_H);
    final JMenuItem about_menu_item = new JMenuItem("About");
    about_menu_item.setMnemonic(KeyEvent.VK_A);
    about_menu_item.setToolTipText("View the about dialog");
    the_help_menu.add(about_menu_item);
    about_menu_item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_event) {
        final StringBuilder sb = new StringBuilder();
        sb.append(GREETING_TEXT_2);
        sb.append(GREETING_TEXT_3);
        sb.append(GREETING_TEXT_4);
        sb.append(GREETING_TEXT_5);
        sb.append(GREETING_TEXT_6);
        sb.append(GREETING_TEXT_7);
        sb.append(GREETING_TEXT_8);
        sb.append(GREETING_TEXT_9);
        sb.append(GREETING_TEXT_13);
        sb.append(GREETING_TEXT_10);
        sb.append(GREETING_TEXT_11);
        sb.append(GREETING_TEXT_12);
        sb.append("TCSS 305 Tetris, v1.0");
        javax.swing.JOptionPane.showMessageDialog(null, sb);
      }
    });
  }

  /**
   * Create menu bar using helper methods. Create ButtonGroup for tool menu.
   * @return JMenuBar
   */
  private JMenuBar createMenuBar() { 
    final JMenuBar menu_bar = new JMenuBar();
    final JMenu file_menu = new JMenu("File");
    setupFileMenuItems(file_menu);
    
    final JMenu options_menu = new JMenu("Options");
    options_menu.setMnemonic(KeyEvent.VK_O);
    final JMenu board_size_menu = new JMenu("Board Size");
    options_menu.add(board_size_menu);
    setupBoardSizeMenuItems(board_size_menu);

    final JMenu help_menu = new JMenu("Help");
    setupHelpMenuItems(help_menu);
    
    // add menus to menu bar
    menu_bar.add(file_menu);
    menu_bar.add(options_menu);
    menu_bar.add(help_menu);
        
    return menu_bar;
  }
  
  /**
   * Observable update, get the updated text for score and lines cleared.
   * @param the_source Observable
   * @param the_data Object
   */
  @Override
  public void update(final Observable the_source, final Object the_data) {
    my_score_label.setText("Score: " + my_board.getMyScore() + BLANK);
    my_lines_cleared_label.setText("Lines Cleared: " + my_board.getMyLinesCleared() + BLANK);
    calcDelay();  
    repaint();
  }
  
  /**
   * Calculate the delay of the game timer.
   */
  public void calcDelay() {
    final int desired_delay = SPEED - (my_board.getMyLinesCleared() / REQ_LINES) * DECREMENT;
    // check time to be sure only changing delay when needed
    if (my_timer.getDelay() != desired_delay) {
      my_timer.setDelay(desired_delay);
    }
  }

  /**
   * Empty method.
   * @param the_event KeyEvent
   */
  @Override
  public void keyReleased(final KeyEvent the_event) {
    // Empty method
  }
  
  /**
   * Empty method.
   * @param the_event KeyEvent
   */
  @Override
  public void keyTyped(final KeyEvent the_event) {
    // Empty method
  }
  
  /**
   * Empty method.
   * @param the_event KeyEvent
   */
  @Override
  public void actionPerformed(final ActionEvent the_event) {
    // Empty method
  }
  
  /**
   * Call movement on board based on KeyEvent.
   * @param the_event KeyEvent
   */
  @Override
  public void keyPressed(final KeyEvent the_event) {
    switch (the_event.getKeyCode()) {
      case KeyEvent.VK_LEFT :
        my_board.moveLeft();
        break;
      case KeyEvent.VK_RIGHT :
        my_board.moveRight();
        break;
      case KeyEvent.VK_DOWN :
        my_board.moveDown();
        break;
      case KeyEvent.VK_ENTER :
        my_board.rotate(); 
        break;
      case KeyEvent.VK_SPACE:
        my_board.dropPiece();
        break;
      case KeyEvent.VK_P:
        my_board.setPaused(true);
        break;
      case KeyEvent.VK_C:
        my_board.setPaused(false);
        break;
      default:
        break;
    }
  }

  /**
   * Nested class for board size action.
   */
  public class BoardSizeAction extends AbstractAction {
  
  /**
   * Instance variable for current board size.
   */
    private final int my_board_size;
  
  /**
   * Constructor.
   * @param the_board_size int
   */
    public BoardSizeAction(final int the_board_size) {
    super();
      my_board_size = the_board_size;
    }
  
    @Override
    public void actionPerformed(final ActionEvent the_event) {
      setBoardSize(my_board_size);
    }
  }
  /**
   * ActionListener class for GameTimer.
   */
  class GameTimer implements ActionListener {
    
    /**
     * ActionEvent for GameTimer.
     * @param the_event ActionEvent
     */
    public void actionPerformed(final ActionEvent the_event) {
      my_board.update();
      if (my_board.isGameOver()) {
        gameOver();
      }
    }
  }  
}

