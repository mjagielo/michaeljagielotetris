package gui;


import board.ImmutablePoint;
import board.TetrisBoard;
import board.TetrisPiece;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;



/*
 * Michael Jagielo
 * 
 * TCSS 305 - Autumn 2011
 * Milestone 3 - Initial GUI
 * 04 December 2011
 */

/**
 * Drawing panel class graphical display of Tetris piece.
 * @author Michael
 * @version 04 December 2011
 *
 */
@SuppressWarnings("serial")
public class PieceDisplayPanel extends JPanel implements Observer {
  
  /**
   * Constant for setting of piece display panel.
   */
  private static final int DEFAULT_SIZE = 70; 
  
  /**
   * Constant for piece size.
   */
  private static final int PIECE_SIZE = 10;
  
  /**
   * Constant for box height.
   */
  private static final int BOX_HEIGHT = 10;
  
  /**
   * Constant for box width.
   */
  private static final int BOX_WIDTH = 50;
  
  /**
   * Constant for location of box that frames next tetris piece.
   */
  private static final int LOCATION_DISPLACEMENT = 20;
  
  /**
   * Instance field for the next tetris piece to be displayed.
   */
  private TetrisPiece my_next_piece;
  
  /**
   * Sentinel for the ordered number of piece. 
   */
  private int my_piece_count;
  
  /**
   * Constructor.
   */
  public PieceDisplayPanel() {
    super();
    setPreferredSize(new Dimension(DEFAULT_SIZE, DEFAULT_SIZE));
    setBackground(Color.LIGHT_GRAY); 
    final TetrisBoard board = new TetrisBoard();
    my_next_piece = board.getNextPiece();
    my_piece_count = 0;
    
  }  
  
  /**
   * Draw on the DrawingPanel.
   * @param the_graphics Graphics
   * @Override
   */
  public void paintComponent(final Graphics the_graphics) {
    super.paintComponent(the_graphics);
    final Graphics2D g2d = (Graphics2D) the_graphics;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2d.setPaint(Color.BLUE);
    g2d.draw(new Rectangle(BOX_HEIGHT, BOX_HEIGHT, BOX_WIDTH, BOX_WIDTH));
    
    // translate points and draw active piece
    final ImmutablePoint[] next_piece_points_array = my_next_piece.getMyPoints();
     
    if (my_piece_count > 0) {
       // draw next piece
      g2d.setPaint(my_next_piece.getColor());
      for (int i = 0; i < next_piece_points_array.length; i++) {
        drawBlock(g2d, next_piece_points_array[i].x() * PIECE_SIZE + LOCATION_DISPLACEMENT,
               next_piece_points_array[i].y() * PIECE_SIZE + LOCATION_DISPLACEMENT,
               PIECE_SIZE, PIECE_SIZE);
      }
    }

  }
  
  /**
   * Draw block for tetris piece.
   * @param the_g integer
   * @param the_x integer
   * @param the_y integer
   * @param the_width integer
   * @param the_height integer
   */
  private void drawBlock(final Graphics2D the_g, final int the_y, final int the_x,
                         final int the_width, final int the_height) {
    the_g.setPaint(my_next_piece.getColor()); 
    the_g.fillRect(the_x, the_y, the_width, the_height);
    the_g.setPaint(Color.BLACK);
    the_g.drawRect(the_x, the_y, the_width, the_height);
  }

  @Override
  public void update(final Observable the_source, final Object the_data) {
    if (the_source instanceof TetrisBoard) {
      my_next_piece = ((TetrisBoard) the_source).getNextPiece();
      my_piece_count++;
    }
    repaint();
  }
}
